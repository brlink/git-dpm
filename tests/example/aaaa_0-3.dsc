Format: 1.0
Source: aaaa
Binary: done
Architecture: all
Version: 0-3
Maintainer: Nobody <nobody@nowhere>
Standards-Version: 3.9.0
Build-Depends: debhelper (>= 5), quilt
Checksums-Sha1: 
 0b76a8741ca3334c168ca55187a7ff5e2fd72f64 352 aaaa_0.orig.tar.gz
 66255c699cbf7273ad29988f1e4d1c9a9eb1e619 1164 aaaa_0-3.diff.gz
Checksums-Sha256: 
 aa6ecb5f14669008b5b9be27a0fa764698f4215d7a928be67ecd3169d65f650a 352 aaaa_0.orig.tar.gz
 02bf8c557df2b96ba0f072d4ea5d97be703b33c92b64c5bdeb3822b393b201ef 1164 aaaa_0-3.diff.gz
Files: 
 e924b00324be1f8194c7115b7fc508fd 352 aaaa_0.orig.tar.gz
 6d3a674c510a427651a0621bde42b0ff 1164 aaaa_0-3.diff.gz
