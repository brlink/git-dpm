2019-02-03 prepare 0.10.0
2019-02-03 switch to generate .tar.xz by default and enable automake's check-news option
2019-02-03 remove all references to alioth from the documentation
2019-02-03 increase the quality of the generated images in the documentation
2019-02-02 record available .asc files into debian/.git-dpm
2019-02-02 check in .asc files into pristine-tar
2019-02-02 on prepare, checkout recorded .asc files
2019-01-27 prepare 0.9.2
2019-01-27 don't set GREP_OPTIONS
2014-10-18 prepare 0.9.1
2014-10-18 describe what --allow-nonlinear does
2014-10-18 fix import-dsc broken by tar_exclude functionality (and add --tar-exclude option to it)
2014-10-11 prepare 0.9.0
2014-10-11 mark new-upstream command name deprecated and make sure all documentation mentions record-new-upstream
2014-10-11 add --exclude option to import-tar and import-new-upstream passed to tar
2014-10-11 fix bug in merge-patched reporting debian/patches up to date if it was empty before
2014-09-07 look into debian/.git-dpm for default tag names
2014-09-07 add %V %U %E %f to tag name templates
2014-07-06 Document how to remove patches in the middle of the series
2014-08-09 try to help against a remote pristine-tar branch not having a local branch tracking it yet
2014-08-09 add a --ignore-unclean-branches to git-dpm import-new-uptream
2014-07-24 allow non-lightweight tags at more places
2014-03-09 prepare 0.8.6
2014-03-09 fix bug in tar importer w.r.t. files with hardlinks in it
2014-03-09 remove double-alias c-p (now only alias for checkout-patched)
2014-03-02 try harder to detect dpatch patches to import
2013-12-28 silence some shelllint warnings
2013-12-28 Some grammar fixes for the manpage
2013-12-28 Update webpage now that git-dpm is packaged
2013-12-28 make index.html valid html
2013-12-28 make example.html valid html
2013-12-27 Allow passing annotated tags to "-p"
2013-08-29 fix bug in file deletion propagation
2013-06-18 fix a lot of spelling mistaked and/or spelling inconsistencies
2013-06-12 allow import-dsc --edit-patches to also edit author information
2013-06-12 also cope with Author: <email> lines in patches to apply
2013-03-21 fix wrong hint in overlapping project error message
2013-03-21 proper error message if new-upstream is called with no new upstream branch
2013-03-21 add alias record-new-upstream for new-upstream
2012-06-11 prepare 0.8.4
2012-06-11 when using import-dsc --use-changelog, also use date as commit date for patches from 1.0 packages
2012-06-09 if a dpatch patch does not apply, try some more git-apply options
2012-05-15 prepare 0.8.3
2012-05-15 manpage: instruct groff to produce hyphen-minus and not hyphen or minus
2012-04-24 prepare 0.8.2
2012-04-24 fix test against git 1.7.1 triggered by git 1.7.10
2012-04-08 add missing -- to some git-dpm dch examples in manpage
2012-04-02 prepare 0.8.1
2012-04-02 fix import-dsc complaining about up-to-date upstream branch
2012-02-22 import-{tar,dsc}: do not call tar with -U as that breaks tar in squeeze with tarballs containing an explicit ./ directory
2012-02-22 import-{tar,dsc}: fix "rmdir: failed to remove `./': Invalid argument"
2012-02-18 git-dpm init: respect --allow-upstream-changes-in-debian
2012-02-04 if not changing the upstream branch but only recording new files, don't call or warn about rebase-patched
2011-11-27 prepare 0.8.0
2011-09-19 add --use-changelog to import-dsc
2011-11-12 don't get confused by dpatch files starting with '#\!/' instead if '#\! /'
2011-11-12 filter out ^diff -[Naurp]* from diff descriptions
2011-10-23 remove buggy cherry-pick error message
2011-10-23 improve dpatch patch header (especially those without author information)
2011-10-01 try to ignore less errors
2011-10-01 explicitly return all errors instead of relying on set -e
2011-10-01 fix typo making warning in import-tar to error
2011-09-29 add -x to test.sh
2011-09-21 add --allow-native-import to import-dsc
2011-09-19 harmonize order verbatim branch is merged in
2011-09-19 refactor code to internally encapsulate git commit-tree calls
2011-09-18 add --author to import-tar, --upstream-author and --upstream-date to import-new-upstream and import-dsc
2011-09-21 import-dsc: don't get confused by .dsc without Format:
2011-09-18 make rebase-patches work with no upstream branch existing as git branch yet
2011-09-16 add empty line after subject so git identifies it correctly
2011-09-13 avoid problems with some awk variants
2011-09-13 avoid one instance of git-checkout being now too verbose in import-dsc
2011-09-08 remove forgotten debug output
2011-08-29 update command list in --help
2011-07-21 rewrite merge operation, add --ignore-deletions, add --dot-git-files, remove unreleased --keep-deletes/--no-deletes/--uptream-git-files/--keep-git-files again
2011-08-08 require at least git 1.7.2
2011-08-07 add --branch option to import-tar
2011-08-07 fix import-dsc doing the dpm.importwithoutparent=false check at the wrong time
2011-07-19 avoid test-case failing if ~/.gitconfig has dpm.importWithoutParent = false
2011-07-04 add --keep-deletes, --no-deletes, --upstream-git-files, --keep-git-files
2011-06-05 use --no-signature in format-patch calls
2011-05-07 add record-dsc command to record a pristine .dsc file with contents
2011-05-07 make tag and ref-tag more chatty by default
2011-05-06 add dpm.importWithoutParent git config so one does not forget -p with import-new-upstream or import-dsc
2011-05-04 prepare 0.7.1
2011-05-04 fix bug calculating wrong file sizes if those are symlinks
2011-03-19 prepare 0.7.0
2011-03-17 improve error handling if import-dsc cannot import some patches
2011-03-17 improve reverting to previous state if started with a detached HEAD
2011-03-17 fix another bug in dpmPatchedBranch config handling
2011-03-16 fix typo causing spurious error in a patched branch named with dpmPatchedBranch config
2011-03-11 add --init and --branch options to import-new-upstream
2011-03-09 change import-dsc to look more like something not imported
2011-03-14 allow branch names with slashes in them
2011-03-09 don't give update-ref a reason most of the time wrong message for the log
2011-03-08 make import-tar (and other -p users) support volatile commits like HEAD better
2011-03-08 add --date option to import-tar
2011-03-05 allow 'NONE' to disable a specific type of tag
2011-03-04 add 'ref-tag' to tag a not checked out version
2011-03-04 fix 'tag' description in manpage
2011-02-23 prepare 0.6.0
2011-02-22 avoid git getting confused when commits happen to fast by waiting between applying patches
2011-02-12 fix bug causing pristine-tar to be called again
2011-02-12 allow to enable --pristine-tar-commit options by git-config setting
2011-02-11 check harder to ensure not calculated branch name is configured for another function
2011-02-08 allow specifying related branches via git-config
2011-02-08 remove undocumented -U, -D and -P switches.
2011-02-06 add git-dpm tag --debian-tag --patched-tag and --upstream-tag and make those and --named setable via git-config
2011-01-31 import-tar: don't litter working directory with empty directories
2011-01-31 add support for symlinks in import-tar
2011-01-30 fix typo in manpage
2011-01-27 improve emtry tree creation by using git mktree
2011-01-08 better check for existance and correct type of some arguments
2010-12-29 git-dpm.1: fix some typos
2010-11-09 prepare 0.5.0
2010-11-09 add missing ']' in import-dsc (introduces recently)
2010-11-09 add some sanity checks to catch some of the errors possible with components
2010-11-08 don't refuse to record upstream-branch/files if only components changed
2010-11-07 add options to create Patch-Category and Patch-Name fields
2010-11-07 patches can chose their name with Patch-Filename and Patch-Category
2010-11-07 fix newly introduced bug in git-dpm init
2010-11-06 document dch in git-dpm --help
2010-11-06 add --component option to init
2010-11-06 refactor some code (especialy init), remove init --no-commit
2010-11-06 import-dsc now marks patches as exported if there are none
2010-11-05 avoid creating extra commit for .orig.tar where possible
2010-11-05 add components support to git-dpm import-new-upstream
2010-11-05 add --pristine-tar-commit option to import-dsc
2010-11-05 fix import-dsc with components being removed since last revision
2010-11-05 git-dpm empty-tree checks before doing anything
2010-11-04 add support for .orig-component.tar files to import_dsc
2010-11-03 better cope with author information in patches that are not in a format git supports.
2010-11-03 allow imported patches to reside in subdirectories
2010-11-03 better and more checks against upstream changes in the debian branch
2010-11-02 document --component option of new-upstream
2010-11-01 add some support for components
2010-10-31 fix some hypen vs minus issues in the manpage
2010-10-31 prepare 0.4.0
2010-10-31 be less noisy when switching branches
2010-10-31 (import-)new-upstream do a merged-debian if there were no patches previously
2010-10-21 add git-dpm dch to the manpage
2010-10-20 improve update-patches
2010-10-23 improve manpage w.r.t. merge-patched
2010-10-21 git-dpm dch no longer needs a clean debian branch when merging patches
2010-10-01 add some shelllint hints
2010-10-20 add support for deleting stuff in modify_tree
2010-10-20 add --no-rebase-patched option to import-new-upstream
2010-10-20 add -m option to import-new-upstream, new-upstream and merge-patched
2010-10-18 remove confusing rm message from dsc importing code
2010-10-18 remove confusing messages from merge_patched
2010-10-18 fix some newline issues in the new merge_patched
2010-10-15 move git hash-object into function
2010-10-12 implement recording new upstream versions in a more low-level fashion
2010-10-18 make sure .git/dpm/oldcontrol is up to date
2010-10-07 add git-dpm version (only having git-dpm version is perhaps a bit non-standard)
2010-10-07 fix typo
2010-10-06 testsuite: also ignore done.substvars (so it runs on unstable again)
2010-10-06 testsuite: check if git-dpm prepare can checkout a orig.tar file
2010-10-06 prepare 0.3.0
2010-10-05 improve creating of empty commit by using GIT_INDEX_FILE
2010-10-05 Merging patches into debian by using a different index
2010-10-05 always keep control file
2010-08-15 introduce git-dpm dch
2010-10-06 fix merge-patched: proper diagnotics and cleanup if nothing to do
2010-10-04 remove unused variables
2010-10-04 make --date option work
2010-10-03 remove unused --force option of merge-patched
2010-10-03 move lines_must_match into top-level functions calling a function needing it
2010-10-03 fix typo in variable name (only fixing an confusing message, though)
2010-10-03 fix typo causing --amend no effect with git-dpm's merge-patched
2010-10-03 fix wrong variable in debug output and warnings of git-dpm init
2010-10-03 fix typo causing wrong old commits computed in git-dpm import
2010-10-03 remove wrong reverence of level in apply_next_dpatch_patch
2010-10-02 fix miswritten variable name
2010-08-15 improve internal merge_patched code
2010-08-15 improve update-patches
2010-08-15 improve error handling
2010-09-01 do not execute git rev-parse --is-* output but compare to true
2010-10-01 fix typo in variable name
2010-08-23 fix empty-tree
2010-08-20 add link to the Debian Wiki to the documentation
2010-08-20 add git-dpm empty-tree (for test-suite)
2010-08-16 more tests
2010-08-16 fix git-dpm init in the case the upstrem branch does not yet exist
2010-08-03 list import-dsc in commands printed by --help
