git-dpm: manage debian source packages in git
---------------------------------------------

Git-dpm help you store debian source packages with modifications
to the upstream sources in git.

Let's look at an short example. Assume there is some project under
git-dpm control (there is a debian/.git-dpm file).

First get the master branch:
 git clone <URL>

Then create upstream branch and see if the .orig.tar is ready:
 git-dpm prepare

Create the patched branch and check it out:
 git-dpm checkout-patched

Do some changes, apply some patches, commit them..
 <...>
 git commit

If your modification fixes a previous change (and that is not the
last commit, otherwise you could have used \-\-amend),
you might want to squash those two commits into one, so use:
 git rebase -i upstream

Merge your changes into the debian branch and create patches:
 git-dpm update-patches
 dch -i
 git commit --amend -a

Then push the whole thing back:
 git push
